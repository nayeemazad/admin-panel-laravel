<?php 
if (! function_exists('checkPermission')) {  
    function checkPermission($permission) {
        // ...
        $user = Auth::user();
        $roles = $user->roles;
        $permissions = $roles[0]->permissions;
        foreach ($permissions as $p) {
            if ($p->slug == $permission) {
                return true;
                }
            }
            return false;
    }
}
if (! function_exists('checkRole')) {  
    function checkRole(array $role) {
        // ...
        if(Auth::check()){
        $user = Auth::user();
        $roles = $user->roles;
        foreach ($roles as $r) {
            if (in_array($r->name, $role)) {
                return true;
                }
            }
            return false;
    }
    return false;
}
}
if (! function_exists('modules')) {  
    function modules() {
        // ...
        if(Auth::check()){
        $user = Auth::user();
        $roles = $user->roles;
        $modules = $roles[0]->modules;
        foreach ($modules as $m) {
          echo '<li><a href="/{{url($m->slug)}}">{{$m->name}}</a></li>';
            
            }
    }
}
}
if (! function_exists('check_active')) {  
    function check_active($a,$b) {
        // ...
        if($a==$b){
            return 'class="nav-link active"';
        }else{
            return 'class="nav-link"';
        }
     }
    }
?>