<?php

namespace App\Http\Controllers\backend;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Role;
use App\Models\UserRole;
use Illuminate\Support\Facades\Hash;
use Session;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    { //authorize check
       if(!checkPermission('user/list')){
        session()->flash('failed','Permission Access denied.');
        if ($request->expectsJson()) {
            $response = array(
            'status' => 'failed',
            'redirectUrl' => '/adminpanel/user/home',
            'message' => 'You do not have permission for this.', );
         return response()->json($response); 

        }else{
            return redirect()->route('user.home');
        }
       }//end of authorize
        if($request->expectsJson()){
            $users=User::paginate(2);
        return view('backend.user._index',compact('users'));

        }
        $users=User::paginate(2);
        return view('backend.user.index',compact('users'));
    }

     public function loginform()
    {
        return view('backend.user.login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //authorize check
       if(!checkPermission('user/create')){
        session()->flash('failed','Permission Access denied.');
        if ($request->expectsJson()) {
            $response = array(
            'status' => 'failed',
            'redirectUrl' => '/adminpanel/user/home',
            'message' => 'You do not have permission for this.', );
         return response()->json($response); 

        }else{
            return redirect()->route('user.home');
        }
       }//end of authorize
        $roles = Role::all();
        if($request->ajax()){
            return view('backend.user._create', compact('roles'))->render();
        }
        return view('backend.user.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {//authorize check
       if(!checkPermission('user/create')){
        session()->flash('failed','Permission Access denied.');
        if ($request->expectsJson()) {
            $response = array(
            'status' => 'failed',
            'redirectUrl' => '/adminpanel/user/home',
            'message' => 'You do not have permission for this.', );
         return response()->json($response); 

        }else{
            return redirect()->route('user.home');
        }
       }//end of authorize
        //validate
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'username' => 'required|unique:users|min:5|max:30',
            'password' => 'required|min:8|max:20',
            'role' => 'required',
        ]);
       
        $item= new User();
        $item->name=$request->name;
        $item->email=$request->email;
        $item->username=$request->username;
        $item->password=bcrypt($request->password);

        if ($item->save()) {
            $userRole=new UserRole();
            $userRole->role_id=$request->role;
            $userRole->user_id=$item->id;
            if($userRole->save()){
          $response = array(
          'status' => 'success',
          'message' => 'User Registered successfully', );
         return response()->json($response);
            }else{
                $response = array(
          'status' => 'failed',
          'message' => 'Something went wrong.try again', );
         return response()->json($response);
            }
         
             
        } else {
            $response = array(
          'status' => 'failed',
          'message' => 'Something went wrong. try again.', );
         return response()->json($response);
        }
    }

    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        //authorize check
       if(!checkPermission('user/editRole')){
        session()->flash('failed','Permission Access denied.');
        if ($request->expectsJson()) {
            $response = array(
            'status' => 'failed',
            'redirectUrl' => '/adminpanel/user/home',
            'message' => 'You do not have permission for this.', );
         return response()->json($response); 

        }else{
            return redirect()->route('user.home');
        }
       }//end of authorize

        $user=User::find($id);
        $uRole=$user->roles;
        $roles=Role::all();
        //protect super user
        $auth_user = Auth::user();
        $auth_roles = $auth_user->roles;   
        if($uRole[0]->slug == "super-admin"){
            if($auth_roles[0]->slug != "super-admin"){
            session()->flash('failed','Permission Access denied. You can not change SUPER USER !!');
        if ($request->expectsJson()) {
            $response = array(
            'status' => 'failed',
            'redirectUrl' => '/adminpanel/user/home',
            'message' => 'Permission Access denied. You can not change SUPER USER !!', );
         return response()->json($response); 

        }else{
            return redirect()->route('user.home');
        }
            }
        }

         if ($request->ajax()) {             
            return view('backend.user._edit', compact('user','roles'))->render();           
        }else{
            return view('backend.user.edit',compact('user','roles'));
        }
    }

    public function editRole(Request $request,$id)
    {//authorize check
       if(!checkPermission('user/editRole')){
        session()->flash('failed','Permission Access denied.');
        if ($request->expectsJson()) {
            $response = array(
            'status' => 'failed',
            'redirectUrl' => '/adminpanel/user/home',
            'message' => 'You do not have permission for this.', );
         return response()->json($response); 

        }else{
            return redirect()->route('user.home');
        }
       }//end of authorize
        //validate
        $this->validate($request, [
            'role' => 'required',
        ]);

        $item=UserRole::find($id);
        $item->role_id=$request->role;
         if ($item->save()) {             
         if ($request->ajax()) {             
            $response = array(
          'status' => 'success',
          'message' => 'Role has been changed successfully', );
         return response()->json($response);

        }
    }else{              
            $response = array(
          'status' => 'failed',
          'message' => 'Something went wrong', );
         return response()->json($response);
        }
}

    
    public function login(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required',
        ]);
        if (Auth::attempt(['username' => $request->username, 'password' => $request->password])) {
           $response = array(
          'status' => 'success',
          'redirectUrl' => 'adminpanel/user/home',
          'message' => 'Login successful.you will be redirected to home..', );
         return response()->json($response);
        } else {
            $response = array(
          'status' => 'failed',
          'message' => 'username or password is incorrect', );
         return response()->json($response);
        }
    }

    public function logout()
    {
        Auth::logout();
        Session::flash('successMessage', 'Logout Successful...');
        return redirect()->route('user.loginform');
    }
}
