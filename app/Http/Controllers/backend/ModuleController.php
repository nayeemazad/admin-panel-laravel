<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Permission;
use App\Models\Module;
use App\Models\Role;
use App\Models\RolePermission;
use App\Models\RoleModule;
use Auth;
class ModuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //authorize check
       if(!checkPermission('module/list')){
        session()->flash('failed','Permission Access denied.');
        if ($request->expectsJson()) {
            $response = array(
            'status' => 'failed',
            'redirectUrl' => '/adminpanel/user/home',
            'message' => 'You do not have permission for this.', );
         return response()->json($response); 

        }else{
            return redirect()->route('user.home');
        }
       }//end of authorize

        if($request->expectsJson()){
        $modules= Module::paginate(5);
        return view('backend.module._index',compact('modules'))->render();    
        }
        $modules= Module::paginate(5);
        return view('backend.module.index',compact('modules'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //authorize check
       if(!checkPermission('module/create')){
        session()->flash('failed','Permission Access denied.');
        if ($request->expectsJson()) {
            $response = array(
            'status' => 'failed',
            'redirectUrl' => '/adminpanel/user/home',
            'message' => 'You do not have permission for this.', );
         return response()->json($response); 

        }else{
            return redirect()->route('user.home');
        }
       }//end of authorize

    	if($request->expectsJson()){
        return view('backend.module._create');  
        }
        return view('backend.module.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //authorize check
       if(!checkPermission('module/create')){
        session()->flash('failed','Permission Access denied.');
        if ($request->expectsJson()) {
            $response = array(
            'status' => 'failed',
            'redirectUrl' => '/adminpanel/user/home',
            'message' => 'You do not have permission for this.', );
         return response()->json($response); 

        }else{
            return redirect()->route('user.home');
        }
       }//end of authorize
        //validates the data
       $this->validate($request, [
            'name' => 'required',
            'slug' => 'required',
            'rank' => 'required|unique:modules',
        ]);
        
       if ($request->expectsJson()) {
        $item= new Module();
        $item->name=$request->name;
        $item->slug=$request->slug;
        $item->rank=$request->rank;
        if ($item->save()) {
            $response = array(
          'status' => 'success',
          'redirectUrl' => '/adminpanel/module/list',
          'message' => 'created successfully', );
         return response()->json($response); 
        } 
        else {
            $response = array(
          'status' => 'failed',
          'message' => 'Something went wrong.', );
         return response()->json($response);
        }
    }
}

//get module assign form
    public function assign(Request $request,$id)
    {
       //authorize check
       if(!checkPermission('module/assign')){
        session()->flash('failed','Permission Access denied.');
        if ($request->expectsJson()) {
            $response = array(
            'status' => 'failed',
            'redirectUrl' => '/adminpanel/user/home',
            'message' => 'You do not have permission for this.', );
         return response()->json($response); 

        }else{
            return redirect()->route('user.home');
        }
       }//end of authorize
       if ($request->expectsJson()){
        $role = Role::find($id);
        $modules = Module::all();
        $activemodules = $role->modules;
        $response = array(
          'status' => 'success',
          'role' => $role,
          'modules' => $modules,
          'activemodules' => $activemodules,     
          );
         return response()->json($response);
     }else{
        return redirect()->back();
     }
    }
//submit module assign form
    public function assignNow(Request $request, $id)
    {
        //authorize check
       if(!checkPermission('module/assign')){
        session()->flash('failed','Permission Access denied.');
        if ($request->expectsJson()) {
            $response = array(
            'status' => 'failed',
            'redirectUrl' => '/adminpanel/user/home',
            'message' => 'You do not have permission for this.', );
         return response()->json($response); 

        }else{
            return redirect()->route('user.home');
        }
       }//end of authorize

        $data['role_id'] = $id;
        $role = Role::find($id);
        //protect super user
        $auth_user = Auth::user();
        $auth_roles = $auth_user->roles;   
        if($role->slug == "super-admin"){
            if($auth_roles[0]->slug != "super-admin"){
            session()->flash('failed','Permission Access denied. You can not assign SUPER USER modules!!');
        if ($request->expectsJson()) {
            $response = array(
            'status' => 'failed',
            'redirectUrl' => '/adminpanel/user/home',
            'message' => 'Permission Access denied. You can not assign SUPER USER modules!!', );
         return response()->json($response); 

        }else{
            return redirect()->route('user.home');
        }
            }
        }
        //update modules
        $rm = RoleModule::where('role_id', '=', $id)->get();
        foreach ($rm as $r) {
            $r->delete();
        }
        if (isset($request->asignmodule)) {
            foreach ($request->asignmodule as $m) {
                $data['module_id'] = $m;
                RoleModule::create($data);
            }
        } $response = array(
          'status' => 'success',
          'message' => 'module assigned successfully',
          );
         return response()->json($response);
    }

    //delete modules
     public function delete(Request $request,$id)
    {
        //authorize check
       if(!checkPermission('module/delete')){
        session()->flash('failed','Permission Access denied.');
        if ($request->expectsJson()) {
            $response = array(
            'status' => 'failed',
            'redirectUrl' => '/adminpanel/user/home',
            'message' => 'You do not have permission for this.', );
         return response()->json($response); 

        }else{
            return redirect()->route('user.home');
        }
       }//end of authorize

        $module = Module::find($id);
        $module->delete();
        return redirect()->route('module.list')->with('success', 'Deleted successfully');
    }
}
