<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Permission;
use App\Models\Role;
use App\Models\RolePermission;
use Auth;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //authorize check
       if(!checkPermission('permission/list')){
        session()->flash('failed','Permission Access denied.');
        if ($request->expectsJson()) {
            $response = array(
            'status' => 'failed',
            'redirectUrl' => '/adminpanel/user/home',
            'message' => 'You do not have permission for this.', );
         return response()->json($response); 

        }else{
            return redirect()->route('user.home');
        }
       }//end of authorize

        if($request->expectsJson()){
        $permissions= Permission::paginate(10);
        return view('backend.permission._index',compact('permissions'))->render();    
        }
        $permissions= Permission::paginate(10);
        return view('backend.permission.index',compact('permissions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
         //authorize check
       if(!checkPermission('permission/create')){
        session()->flash('failed','Permission Access denied.');
        if ($request->expectsJson()) {
            $response = array(
            'status' => 'failed',
            'redirectUrl' => '/adminpanel/user/home',
            'message' => 'You do not have permission for this.', );
         return response()->json($response); 

        }else{
            return redirect()->route('user.home');
        }
       }//end of authorize


        if($request->expectsJson()){
        return view('backend.permission._create');    
        }
        return view('backend.permission.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         //authorize check
       if(!checkPermission('permission/create')){
        session()->flash('failed','Permission Access denied.');
        if ($request->expectsJson()) {
            $response = array(
            'status' => 'failed',
            'redirectUrl' => '/adminpanel/user/home',
            'message' => 'You do not have permission for this.', );
         return response()->json($response); 

        }else{
            return redirect()->route('user.home');
        }
       }//end of authorize
        //validate
       $this->validate($request, [
            'name' => 'required',
            'slug' => 'required',
        ]);
        
       if ($request->expectsJson()) {
        $item= new Permission();
        $item->name=$request->name;
        $item->slug=$request->slug;
        if ($item->save()) {
            $response = array(
          'status' => 'success',
          'redirectUrl' => '/adminpanel/permission/list',
          'message' => 'created successfully', );
         return response()->json($response); 
        } 
        else {
            $response = array(
          'status' => 'failed',
          'message' => 'Something went wrong.', );
         return response()->json($response);
        }
    }
}

    public function assign(Request $request,$id)
    {
        //authorize check
       if(!checkPermission('permission/assign')){
        session()->flash('failed','Permission Access denied.');
        if ($request->expectsJson()) {
            $response = array(
            'status' => 'failed',
            'redirectUrl' => '/adminpanel/user/home',
            'message' => 'You do not have permission for this.', );
         return response()->json($response); 

        }else{
            return redirect()->route('user.home');
        }
       }//end of authorize

       //accept only ajax request,else return back
       if (!$request->expectsJson()){
        return redirect()->back();
       }
       //now safe to proceed
        $role = Role::find($id);
        $permissions = Permission::all();
        $activepermissions = $role->permissions;
        $response = array(
          'status' => 'success',
          'role' => $role,
          'permissions' => $permissions,
          'activepermissions' => $activepermissions,     
          );
         return response()->json($response);
    }

    public function assignNow(Request $request, $id)
    {
         //authorize check
       if(!checkPermission('permission/assign')){
        session()->flash('failed','Permission Access denied.');
        if ($request->expectsJson()) {
            $response = array(
            'status' => 'failed',
            'redirectUrl' => '/adminpanel/user/home',
            'message' => 'You do not have permission for this.', );
         return response()->json($response); 

        }else{
            return redirect()->route('user.home');
        }
       }//end of authorize
       
        
        $data['role_id'] = $id;
        $role = Role::find($id);

        //protect super user
        $auth_user = Auth::user();
        $auth_roles = $auth_user->roles;   
        if($role->slug == "super-admin"){
            if($auth_roles[0]->slug != "super-admin"){
            session()->flash('failed','Permission Access denied. You can not assign SUPER USER permissions!!');
        if ($request->expectsJson()) {
            $response = array(
            'status' => 'failed',
            'redirectUrl' => '/adminpanel/user/home',
            'message' => 'Permission Access denied. You can not assign SUPER USER permissions!!', );
         return response()->json($response); 

        }else{
            return redirect()->route('user.home');
        }
            }
        }
        //update permissions
        $rp = RolePermission::where('role_id', '=', $id)->get();
        foreach ($rp as $r) {
            $r->delete();
        }
        if (isset($request->asignpermission)) {
            foreach ($request->asignpermission as $p) {
                $data['permission_id'] = $p;
                RolePermission::create($data);
            }
        } $response = array(
          'status' => 'success',
          'message' => 'permission assigned successfully',
          );
         return response()->json($response);
    }

    public function delete(Request $request,$id)
    {
        //authorize check
       if(!checkPermission('permission/delete')){
        session()->flash('failed','Permission Access denied.');
        if ($request->expectsJson()) {
            $response = array(
            'status' => 'failed',
            'redirectUrl' => '/adminpanel/user/home',
            'message' => 'You do not have permission for this.', );
         return response()->json($response); 

        }else{
            return redirect()->route('user.home');
        }
       }//end of authorize

        $permisison = Permission::find($id);
        $permisison->delete();
        return redirect()->route('permission.list')->with('success', 'Deleted successfully');
    }
}
