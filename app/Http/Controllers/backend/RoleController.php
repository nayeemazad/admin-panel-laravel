<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Role;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
  
     public function index(Request $request)
    
    {//authorize check
       if(!checkPermission('role/list')){
        session()->flash('failed','Permission Access denied.');
        if ($request->expectsJson()) {
            $response = array(
            'status' => 'failed',
            'redirectUrl' => '/adminpanel/user/home',
            'message' => 'You do not have permission for this.', );
         return response()->json($response); 

        }else{
            return redirect()->route('user.home');
        }
       }//end of authorize

        if($request->expectsJson()){
        $roles= Role::paginate(2);
        return view('backend.role._index',compact('roles'))->render();
        }
        $roles= Role::paginate(2);
        return view('backend.role.index',compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    { //authorize check
       if(!checkPermission('role/create')){
        session()->flash('failed','Permission Access denied.');
        if ($request->expectsJson()) {
            $response = array(
            'status' => 'failed',
            'redirectUrl' => '/adminpanel/user/home',
            'message' => 'You do not have permission for this.', );
         return response()->json($response); 

        }else{
            return redirect()->route('user.home');
        }
       }//end of authorize

         if($request->expectsJson()){
            return view('backend.role._create');
         }
        return view('backend.role.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { //authorize check
       if(!checkPermission('role/create')){
        session()->flash('failed','Permission Access denied.');
        if ($request->expectsJson()) {
            $response = array(
            'status' => 'failed',
            'redirectUrl' => '/adminpanel/user/home',
            'message' => 'You do not have permission for this.', );
         return response()->json($response); 

        }else{
            return redirect()->route('user.home');
        }
       }//end of authorize

//validates the data
       $this->validate($request, [
            'name' => 'required',
            'slug' => 'required|unique:roles',
        ]);
        
       if ($request->expectsJson()) {
        $item= new Role();
        $item->name=$request->name;
        $item->slug=$request->slug;
        if ($item->save()) {
            $response = array(
          'status' => 'success',
          'redirectUrl' => '/adminpanel/role/list',
          'message' => 'Role created successfully', );
         return response()->json($response); 
        } 
        else {
            $response = array(
          'status' => 'failed',
          'message' => 'Something went wrong.', );
         return response()->json($response);
        }
    }
}


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
