<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Permission;
use App\Models\Module;
use App\Models\Role;
use App\Models\User;
class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        //authorize check
       if(!checkPermission('user/home')){
        session()->flash('failed','Permission Access denied.');
        if ($request->expectsJson()) {
            $response = array(
            'status' => 'failed',
            'redirectUrl' => '/adminpanel/user/home',
            'message' => 'You do not have permission for this.', );
         return response()->json($response); 

        }else{
            return redirect()->route('user.loginform');
        }
       }//end of authorize
       $total_user=User::count();
       $total_role=Role::count();
       $total_permission=Permission::count();
       $total_module=Module::count();
        return view('backend.dashboard.index',compact('total_user','total_role','total_permission','total_module'));
    }


}
