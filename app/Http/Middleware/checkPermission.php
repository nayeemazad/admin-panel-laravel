<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class checkPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next,$permission)
    { 
        if(Auth::check()){
        $user = Auth::user();
        $roles = $user->roles;
        $permissions = $roles[0]->permissions;
        foreach ($permissions as $p) {
            if ($p->slug == $permission) {
                return $next($request);
                }
        }
        if ($request->ajax()) {
          $response = array(
          'status' => 'failed',
          'redirectUrl' => '/adminpanel/user/home',
          'message' => 'You do not have permission for this.', );
         return response()->json($response); 

        }else{
            return redirect()->route('user.home')->with('failed','You do not have permission for this');
        }
    }else{
        return redirect()->route('user.loginform')->with('failed','You do not have permission for this');
    }
    }
}
