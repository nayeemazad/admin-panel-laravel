<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    //
    function  permissions(){
        return $this->belongsToMany('App\Models\Permission', 'role_permissions');
    }function  modules(){
        return $this->belongsToMany('App\Models\Module', 'role_modules');
    }
}
