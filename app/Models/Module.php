<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    //
    function  roles(){
        return $this->belongsToMany('App\Models\Role', 'role_modules');
    }
}
