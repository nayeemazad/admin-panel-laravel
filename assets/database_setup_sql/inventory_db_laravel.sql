-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 10, 2018 at 07:36 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inventory_db_laravel`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_11_02_144617_create_roles_table', 1),
(4, '2018_11_02_145024_create_user_roles_table', 1),
(5, '2018_11_03_090659_create_permissions_table', 1),
(6, '2018_11_03_090849_create_role_permissions_table', 1),
(7, '2018_11_09_160619_create_modules_table', 1),
(8, '2018_11_09_160648_create_role_modules_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rank` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `created_at`, `name`, `slug`, `rank`, `updated_at`) VALUES
(1, '2018-11-09 10:41:19', 'User Management', 'adminpanel/user/list', '2', '2018-11-09 10:41:19'),
(2, '2018-11-09 10:42:15', 'Role Management', 'adminpanel/role/list', '3', '2018-11-09 10:42:15'),
(3, '2018-11-09 10:43:05', 'Permission Management', 'adminpanel/permission/list', '4', '2018-11-09 10:43:05'),
(4, '2018-11-09 10:43:41', 'Module Management', 'adminpanel/module/list', '5', '2018-11-09 10:43:41'),
(5, '2018-11-09 14:28:34', 'Dashboard', 'adminpanel/user/home', '1', '2018-11-09 14:28:34'),
(6, '2018-11-09 14:29:56', 'Factory Management', 'adminpanel/factory/list', '6', '2018-11-09 14:29:56'),
(7, '2018-11-09 14:31:43', 'Product Management', 'adminpanel/product/list', '7', '2018-11-09 14:31:43');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'user list', 'user/list', '2018-11-09 13:22:33', '2018-11-09 13:22:33'),
(2, 'User Create', 'user/create', '2018-11-09 14:19:07', '2018-11-09 14:19:07'),
(3, 'permission assign', 'permission/assign', '2018-11-09 14:21:34', '2018-11-09 14:21:34'),
(4, 'dashboard', 'user/home', '2018-10-31 18:00:00', '2018-10-31 18:00:00'),
(5, 'user role edit', 'user/editRole', '2018-10-31 18:00:00', '2018-10-31 18:00:00'),
(6, 'permission list', 'permission/list', '2018-10-31 18:00:00', '2018-10-31 18:00:00'),
(7, 'permission create', 'permission/create', '2018-10-31 18:00:00', '2018-10-31 18:00:00'),
(8, 'module list', 'module/list', '2018-10-31 18:00:00', '2018-10-31 18:00:00'),
(9, 'module create', 'module/create', '2018-10-31 18:00:00', '2018-10-31 18:00:00'),
(10, 'module assign', 'module/assign', '2018-10-31 18:00:00', '2018-10-31 18:00:00'),
(11, 'role list', 'role/list', '2018-10-31 18:00:00', '2018-10-31 18:00:00'),
(12, 'role create', 'role/create', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Super Admin', 'super-admin', '2018-11-09 10:10:30', '2018-11-09 10:10:30'),
(2, 'Admin', 'admin', '2018-11-09 11:15:51', '2018-11-09 11:15:51'),
(3, 'Editor', 'editor', '2018-11-09 13:20:13', '2018-11-09 13:20:13');

-- --------------------------------------------------------

--
-- Table structure for table `role_modules`
--

CREATE TABLE `role_modules` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `module_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_modules`
--

INSERT INTO `role_modules` (`id`, `role_id`, `module_id`, `created_at`, `updated_at`) VALUES
(68, 1, 1, '2018-11-10 11:36:43', '2018-11-10 11:36:43'),
(69, 1, 2, '2018-11-10 11:36:43', '2018-11-10 11:36:43'),
(70, 1, 3, '2018-11-10 11:36:43', '2018-11-10 11:36:43'),
(71, 1, 4, '2018-11-10 11:36:43', '2018-11-10 11:36:43'),
(72, 1, 5, '2018-11-10 11:36:43', '2018-11-10 11:36:43'),
(74, 2, 1, '2018-11-10 11:37:14', '2018-11-10 11:37:14'),
(75, 2, 2, '2018-11-10 11:37:14', '2018-11-10 11:37:14'),
(76, 2, 3, '2018-11-10 11:37:14', '2018-11-10 11:37:14'),
(77, 2, 4, '2018-11-10 11:37:14', '2018-11-10 11:37:14'),
(78, 2, 5, '2018-11-10 11:37:14', '2018-11-10 11:37:14'),
(80, 3, 1, '2018-11-10 11:37:39', '2018-11-10 11:37:39'),
(81, 3, 2, '2018-11-10 11:37:39', '2018-11-10 11:37:39'),
(82, 3, 3, '2018-11-10 11:37:39', '2018-11-10 11:37:39'),
(83, 3, 4, '2018-11-10 11:37:39', '2018-11-10 11:37:39'),
(84, 3, 5, '2018-11-10 11:37:39', '2018-11-10 11:37:39');

-- --------------------------------------------------------

--
-- Table structure for table `role_permissions`
--

CREATE TABLE `role_permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_permissions`
--

INSERT INTO `role_permissions` (`id`, `role_id`, `permission_id`, `created_at`, `updated_at`) VALUES
(111, 1, 1, '2018-11-10 11:01:50', '2018-11-10 11:01:50'),
(112, 1, 2, '2018-11-10 11:01:50', '2018-11-10 11:01:50'),
(113, 1, 3, '2018-11-10 11:01:50', '2018-11-10 11:01:50'),
(114, 1, 4, '2018-11-10 11:01:50', '2018-11-10 11:01:50'),
(115, 1, 5, '2018-11-10 11:01:50', '2018-11-10 11:01:50'),
(116, 1, 6, '2018-11-10 11:01:50', '2018-11-10 11:01:50'),
(117, 1, 7, '2018-11-10 11:01:50', '2018-11-10 11:01:50'),
(118, 1, 8, '2018-11-10 11:01:50', '2018-11-10 11:01:50'),
(119, 1, 9, '2018-11-10 11:01:50', '2018-11-10 11:01:50'),
(120, 1, 10, '2018-11-10 11:01:51', '2018-11-10 11:01:51'),
(121, 1, 11, '2018-11-10 11:01:51', '2018-11-10 11:01:51'),
(122, 1, 12, '2018-11-10 11:01:51', '2018-11-10 11:01:51'),
(123, 2, 1, '2018-11-10 11:12:29', '2018-11-10 11:12:29'),
(124, 2, 2, '2018-11-10 11:12:29', '2018-11-10 11:12:29'),
(125, 2, 3, '2018-11-10 11:12:29', '2018-11-10 11:12:29'),
(126, 2, 4, '2018-11-10 11:12:29', '2018-11-10 11:12:29'),
(127, 2, 5, '2018-11-10 11:12:29', '2018-11-10 11:12:29'),
(128, 2, 6, '2018-11-10 11:12:29', '2018-11-10 11:12:29'),
(129, 2, 7, '2018-11-10 11:12:29', '2018-11-10 11:12:29'),
(130, 2, 8, '2018-11-10 11:12:29', '2018-11-10 11:12:29'),
(131, 2, 9, '2018-11-10 11:12:29', '2018-11-10 11:12:29'),
(132, 2, 10, '2018-11-10 11:12:29', '2018-11-10 11:12:29'),
(133, 2, 11, '2018-11-10 11:12:29', '2018-11-10 11:12:29'),
(134, 2, 12, '2018-11-10 11:12:29', '2018-11-10 11:12:29'),
(135, 3, 1, '2018-11-10 11:14:04', '2018-11-10 11:14:04'),
(136, 3, 2, '2018-11-10 11:14:04', '2018-11-10 11:14:04'),
(137, 3, 4, '2018-11-10 11:14:04', '2018-11-10 11:14:04'),
(138, 3, 6, '2018-11-10 11:14:04', '2018-11-10 11:14:04'),
(139, 3, 7, '2018-11-10 11:14:04', '2018-11-10 11:14:04'),
(140, 3, 8, '2018-11-10 11:14:04', '2018-11-10 11:14:04'),
(141, 3, 9, '2018-11-10 11:14:04', '2018-11-10 11:14:04'),
(142, 3, 11, '2018-11-10 11:14:04', '2018-11-10 11:14:04'),
(143, 3, 12, '2018-11-10 11:14:04', '2018-11-10 11:14:04');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `username`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Nayeem Azad', 'azad.nm0@gmail.com', 'super-admin', '$2y$10$GJutKig/7cEV5AyD2wnM4eC6NDluOx.KbhZK4QoXgRgTLLIFy9qda', 'X7PTRtO9fWrUItQmWxAO6s5iHEJwJ73vgZtbl7MMMGUwEMma2WYIPEimkGmy', '2018-11-09 10:11:30', '2018-11-09 10:11:30'),
(2, 'Nayeem Azad', 'admin@nayeemazad.com', 'admin1234', '$2y$10$mXiKB4qtV2.DVxypaz1RHusUUblIPO21U7OfmiuD9elT.V2.ER99C', 'g75AndSAp3QVHvhyaDYtR0IIeDNShYNyybXVSbouPShNUmN4R8VuID8UMb0a', '2018-11-09 12:18:59', '2018-11-09 12:18:59'),
(3, 'Nayeem Azad', 'test@test.com', 'editor1234', '$2y$10$wF7obI8XM75SPSxW3c1FBeErfabdpba183fjvytFyI4sn0HhhD5t.', NULL, '2018-11-09 12:38:58', '2018-11-09 12:38:58');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`id`, `role_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2018-11-09 10:11:30', '2018-11-10 08:52:30'),
(2, 2, 2, '2018-11-09 12:19:00', '2018-11-09 12:19:00'),
(3, 3, 3, '2018-11-09 12:38:58', '2018-11-10 08:46:14');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`),
  ADD UNIQUE KEY `permissions_slug_unique` (`slug`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_slug_unique` (`slug`);

--
-- Indexes for table `role_modules`
--
ALTER TABLE `role_modules`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_modules_role_id_foreign` (`role_id`),
  ADD KEY `role_modules_module_id_foreign` (`module_id`);

--
-- Indexes for table `role_permissions`
--
ALTER TABLE `role_permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_permissions_role_id_foreign` (`role_id`),
  ADD KEY `role_permissions_permission_id_foreign` (`permission_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_username_unique` (`username`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_roles_role_id_foreign` (`role_id`),
  ADD KEY `user_roles_user_id_foreign` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `role_modules`
--
ALTER TABLE `role_modules`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;
--
-- AUTO_INCREMENT for table `role_permissions`
--
ALTER TABLE `role_permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=144;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `user_roles`
--
ALTER TABLE `user_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `role_modules`
--
ALTER TABLE `role_modules`
  ADD CONSTRAINT `role_modules_module_id_foreign` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_modules_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_permissions`
--
ALTER TABLE `role_permissions`
  ADD CONSTRAINT `role_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
