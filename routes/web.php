<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','backend\UserController@loginform' )->name('user.loginform');
Route::post('user/login','backend\UserController@login')->name('user.login');

Route::group(['middleware' => 'Authenticate'],function() {

Route::prefix('adminpanel')->group(function() {
//user
Route::get('user/edit/{id}','backend\UserController@edit')->name('user.edit');
Route::put('user/editRole/{id}','backend\UserController@editRole')->name('user.editRole');
Route::get('user/logout','backend\UserController@logout')->name('user.logout');
Route::get('user/create','backend\UserController@create')->name('user.create');
Route::get('user/list','backend\UserController@index')->name('user.list');
Route::post('user/store','backend\UserController@store')->name('user.store');
Route::get('user/home','backend\DashboardController@index')->name('user.home');

//role
Route::get('role/create','backend\RoleController@create')->name('role.create');
Route::post('role/store','backend\RoleController@store')->name('role.store');
Route::get('role/list','backend\RoleController@index')->name('role.list');

//permissions
Route::get('permission/create','backend\PermissionController@create')->name('permission.create');
Route::post('permission/store','backend\PermissionController@store')->name('permission.store');
Route::get('permission/list','backend\PermissionController@index')->name('permission.list');
Route::get('permission/assign/{id}','backend\PermissionController@assign')->name('permission.assign');
Route::post('permission/assignNow/{id}','backend\PermissionController@assignNow')->name('permission.assignNow');
Route::delete('permission/delete/{id}','backend\PermissionController@delete')->name('permission.delete');

//modules
Route::get('module/create','backend\ModuleController@create')->name('module.create');
Route::post('module/store','backend\ModuleController@store')->name('module.store');
Route::get('module/list','backend\ModuleController@index')->name('module.list');
Route::get('module/assign/{id}','backend\ModuleController@assign')->name('module.assign');
Route::post('module/assignNow/{id}','backend\ModuleController@assignNow')->name('module.assignNow');
Route::delete('module/delete/{id}','backend\ModuleController@delete')->name('module.delete');
});
});
