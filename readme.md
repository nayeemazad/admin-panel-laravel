## About admin-panel-Laravel

This is a role and permission based laravel admin panel best suited for any kind of customised software to get started. Using this system software users role, permissions, modules access level can be controlled dynamically.

## Project Demo Url:  
https://adminpanel.nayeemazad.com 

## Login credentials: 
username = admin1234 & Password = admin1234 

## Backend: 
Laravel 5.5, PHP, MySQL 

## Frontend: 
Ajax, JS, HTML5, CSS3, Bootstrap 4, AdminLte 3.0.

