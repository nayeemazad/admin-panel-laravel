<table class="table table-condensed">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Module Name</th>
                                    <th>Slug</th>
                                    <th>created date</th>
                                    <th>updated date</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($modules as $module)
                                    <tr>
                                        <td> {{$module->id}}</td>
                                        <td> {{$module->name}}</td>
                                        <td> {{$module->slug}}</td>
                                        <td> {{$module->created_at}}</td>
                                        <td> {{$module->updated_at}}</td>
                                        <td>
                                        <form action="{{route('module.delete',$module->id)}}" method="post">
                                        {{method_field('DELETE')}}
                                        {{ csrf_field()}}
                                        <button class="btn btn-danger btn-sm" type="submit" onclick= "return confirm('are you sure to delete?')"><i class="fa fa-trash"></i></button>
                                        </form>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
{!!$modules->links();!!}
                            </table>