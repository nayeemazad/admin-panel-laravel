@extends('backend.layouts.master')
@section('title','Module List')
@section('content')
    <!-- Main content -->
    <section class="content" style="padding-top: 20px;">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
        <div class="col-md-12">
        <div id="addFrm">
           @include('backend.layouts.partials._message')
        </div>
        <div class="card card-info">
                <div class="card-header">
                  <h3 class="card-title">Module List</h3>
                  </div>
          <div class="card-body"> 
          <a href="{{route('module.create')}}" class="btn btn-primary btn-sm btnAdd" style="float: right;" onclick="return false;"> <i class="fa fa-plus"></i> add </a>
                  
          <div id="modules">
         
              @include('backend.module._index')
          </div>
          
          </div>
          </div>
        </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection
@section('scripts')

 <script>
  //ajax pagination 
  $(function(){
     $('body').on('click', '.pagination a', function(e) { 
      e.preventDefault();
       var url = $(this).attr('href'); 
       var tp = 'GET';
          $.ajax({
             url: url,
             type: tp,
             success: function (data) {
             $('#modules').html(data);
              window.history.pushState("", "", url); 
             },
             error:function(data){
             alert('something went wrong..');
             }
          });//end ajax function
    
    }); //end of ajax pagination
  });
  </script>
     <script>
     //get add module form
  $(function(){
     $('body').on('click', '.btnAdd', function(e) { 
      e.preventDefault();
      $('#addFrm').show();
       var url = $(this).attr('href'); 
       var tp = 'GET';
          $.ajax({
             url: url,
             type: tp,
             success: function (data) {
              if(data.status=='failed'){
                      var message = "<div class='alert alert-warning'>" + data.message + "</div>";
                      $('#addFrm').html(message);
                      }
                      else{
                          $('#addFrm').html(data);
                    }
               
             },
             error:function(data){
             alert('something went wrong..');
             }
          });//end ajax function
    
    }); 

  });

//submit add module form and refresh module table
            $('body').on('submit','#form',function (e) {
                e.preventDefault();
                var url = $(this).attr('action');
                var post = $(this).attr('method');
                var data = $(this).serialize();
                $.ajax({
                    url: url,
                    type: post,
                    data: data,
                    success: function (data) {
                        if(data.status=='success'){
                        var message = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h5><i class="icon fa fa-check"></i> Success!</h5>'+data.message+'</div>';
                      }
                        else if(data.status=='failed'){
                            var message = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h5><i class="icon fa fa-warning"></i> Failed!</h5>'+data.message+'</div>';
                        }
                        $('#addFrm').html(message);
                        refresh_modules();
                    },

                    error:function(data){
                        var errors=data.responseJSON;
                        console.log(errors.errors);
                        var error='<b>Validation Errors!</b>';
                        
                        $.each(errors.errors,function(key ,value){
                            error += "<li>"+value+"</li>";

                        });
                        var message = "<div class='alert alert-danger'><ul>" + error + "</ul></div>";
                        $('#status').html(message);

                    }
                });
            });
    //refresh modules table
function refresh_modules(){
    var url = '/adminpanel/module/list'
       var tp = 'GET';
          $.ajax({
             url: url,
             type: tp,
             success: function (data) {
             $('#modules').html(data);
               
             },
             error:function(data){
             alert('something went wrong..');
             }
          });//end ajax function
    }
  </script>
@endsection