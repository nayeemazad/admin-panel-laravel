<table class="table table-condensed">
                                  <thead>
                                  <tr>
                                      <th>#</th>
                                      <th>Role Name</th>
                                      <th>created date</th>
                                      <th>updated date</th>
                                      <th>Action</th>
                                  </tr>
                                  </thead>
                                  <tbody>
                                  <?php $i=1 ;?>
                                  @foreach($roles as $role)
                                      <tr>
                                          <td id="roleid"> {{$role->id}}</td>
                                          <td> {{$role->name}}</td>
                                          <td> {{$role->created_at}}</td>
                                          <td> {{$role->updated_at}}</td>
                                          <td>
                                          <a id="btn{{$role->id}}" href="{{route('permission.assign',$role->id)}}" class="btn btn-primary btn-sm btnShowPermissionForm" onclick="return false;">Assign permission</a>
                                          <a href="{{route('module.assign',$role->id)}}" class="btn btn-info btn-sm btnShowModuleForm" onclick="return false;">Assign module</a>
                                          
                                          </td>
                                      </tr>
                                  @endforeach
                                  </tbody>
                                  {!!$roles->links();!!}
                              </table>