  @extends('backend.layouts.master')
  @section('title','Role List')
  @section('content')
  <style type="text/css">
      #html{
          background: #fff;
          padding: 20px 20px;
      }
  </style>
      <!-- Main content -->
      <section class="content" style="padding-top: 20px;">
        <div class="container-fluid">
          <!-- Small boxes (Stat box) -->
          <div class="row">
          <div class="col-md-12">
          
        
        
        <div id="addFrm">
        <div id="status">  </div>
        </div>

           <form id="frmAssignPermission" method="post">
              {{ csrf_field()}}
               <div id="load"></div>
          </form>

            <form id="frmAssignModule" method="post">
              {{ csrf_field()}}
               <div id="load_module"></div>
          </form>
              
          
  <div class="card card-info">
                <div class="card-header">
                  <h3 class="card-title">Role List</h3>
                  </div>
                 
                  
          <div  class="card-body"> 
           <a href="{{route('role.create')}}" class="btn btn-primary btn-sm btnAdd" style="float: right;" onclick="return false;"> <i class="fa fa-plus"></i> add </a>
                  
          <div id="roles">
          @include('backend.role._index')  
          </div>
          
          
     </div>
       </div>
          </div>
          </div>
          <!-- /.row -->
        </div><!-- /.container-fluid -->
      </section>
      <!-- /.content -->

  @endsection
  @section('scripts')
  <script>
  //js for assign permission div show if btn clicked
  $(function(){
      $('body').on('click', '.btnShowPermissionForm', function(e) { 
                    e.preventDefault();
                    $('#load').show();
                    $('#load_module').hide();
                    $('#addFrm').hide();
                     var url = $(this).attr('href');
                     var tp ="GET" 
                     $.ajax({
                      url: url,
                      type: tp,
                      success: function (data) {
                          if(data.status=='success'){
                          var r= data.role;
                          var p= data.permissions;
                          var ap= data.activepermissions;
                          console.log(p);
                          var i,j,check;
                          var html='<div id="hide" class="card card-info"> <div class="card-header"><h3 class="card-title">Assign Pernissions</h3><div class="card-tools"><button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button><button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-times"></i></button></div></div><div class="card-body"> <div id="html"> <b>'+r.name+'  permissions</b><br>';
                          for (i = 0; i < p.length; ++i) {
                          for (j = 0; j < ap.length; ++j) {
                              if(p[i].slug==ap[j].slug){
                                  check='checked';
                                  
                              }          
                          }
                          html+='<input type="checkbox" id="'+p[i].name+'" name="asignpermission[]" value="'+p[i].id+'" '+check+'><label for="'+p[i].name+'">'+p[i].name+' </label>&nbsp;&nbsp;&nbsp;';
                          check='';
                      }
                      html+='<input type="hidden" name="id" id="id" value="'+r.id+'"> <button type="submit" name="btnCreate" class="btn btn-sm btn-info">Assign permissions</button></div></div></div>';
                      $('#load').html(html);
                      $('#load').show();
                      
                  }else if(data.status=='failed'){
                      var message = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h5><i class="icon fa fa-warning"></i> Failed!</h5>'+data.message+'</div>';
                           $('#load').html(message);
                  }
              },
                      error:function(data){
                         alert('something went wrong..');
                      }
                  });//end ajax function
            });//end btn click
        });//end fn
   

//js for module form show
$(function(){
$('body').on('click', '.btnShowModuleForm', function(e) { 
                    e.preventDefault();
                    $('#load').hide();
                    $('#load_module').hide();
                    $('#addFrm').hide();
                     var url = $(this).attr('href');
                     var tp ="GET" 
                     $.ajax({
                      url: url,
                      type: tp,
                      success: function (data) {
                          if(data.status=='success'){
                          var r= data.role;
                          var m= data.modules;
                          var am= data.activemodules;
                          console.log(m);
                          console.log(r);
                          console.log(am);
                          var i,j,check;
var html='<div id="hide" class="card card-info"> <div class="card-header"><h3 class="card-title">Assign Modules</h3><div class="card-tools">       <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button><button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-times"></i></button></div></div><div class="card-body"> <div id="html"> <b>'+r.name+'  modules</b><br>';
                          for (i = 0; i < m.length; ++i) {
                          for (j = 0; j < am.length; ++j) {
                              if(m[i].slug==am[j].slug){
                                  check='checked';
                                  
                              }          
                          }
                          html+='<input type="checkbox" id="'+m[i].name+'" name="asignmodule[]" value="'+m[i].id+'" '+check+'><label for="'+m[i].name+'">'+m[i].name+' </label>&nbsp;&nbsp;&nbsp;';
                          check='';
                      }
                      html+='<input type="hidden" name="id" id="id" value="'+r.id+'"> <button type="submit" name="btnCreate" class="btn btn-sm btn-info">Assign modules</button></div></div></div>';
                      $('#load_module').html(html);
                      $('#load_module').show();
                      
                  }else if(data.status=='failed'){
                    
                      var message = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h5><i class="icon fa fa-warning"></i> Failed!</h5>'+data.message+'</div>';
                           $('#load_module').html(message);
                            $('#load_module').show();
                  }
              },
                      error:function(data){
                         alert('something went wrong..');
                      }
                  });//end ajax function
    });//end btn click



  }); //end ready function
  </script>
  <script>
  //js for assigned module form submit
  $(document).ready(function(){
       $.ajaxSetup({
                  headers: {
                      'X-CRF-TOKEN': $('meta[name = "csrf-token"]').attr('content')
                  }
              });
       $('#frmAssignModule').on('submit', function (e) {
                    e.preventDefault();
                    var id=document.getElementById('id').value;
                    
                     var url = '/adminpanel/module/assignNow/'+id;
                     var post ="POST" 
                     var data=$(this).serialize();
                     $.ajax({
                      url: url,
                      type: post,
                      data:data,
                      success: function (data) {
                      if(data.status=='success'){
                      var message = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h5><i class="icon fa fa-check"></i> Success!</h5>'+data.message+'</div>';
                    }
                      else if(data.status=='failed'){
                      var message = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h5><i class="icon fa fa-warning"></i> Failed!</h5>'+data.message+'</div>';
                          
                      }
                      $('#load_module').html(message);
              },
                      error:function(data){
                         alert('something went wrong..');
                      }
                  });//end ajax function
    });//end btn click
  }); //end ready function

  //js for assigned permission form submit
  $(document).ready(function(){
       $.ajaxSetup({
                  headers: {
                      'X-CRF-TOKEN': $('meta[name = "csrf-token"]').attr('content')
                  }
              });
       $('#frmAssignPermission').on('submit', function (e) {
                    e.preventDefault();
                    var id=document.getElementById('id').value;
                    
                     var url = '/adminpanel/permission/assignNow/'+id;
                     var post ="POST" 
                     var data=$(this).serialize();
                     $.ajax({
                      url: url,
                      type: post,
                      data:data,
                      success: function (data) {
                      if(data.status=='success'){
                      var message = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h5><i class="icon fa fa-check"></i> Success!</h5>'+data.message+'</div>';
                    }
                      else if(data.status=='failed'){
                      var message = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h5><i class="icon fa fa-warning"></i> Failed!</h5>'+data.message+'</div>';
                          }
                      $('#load').html(message);
              },
                      error:function(data){
                         alert('something went wrong..');
                      }
                  });//end ajax function
    });//end btn click
  }); //end ready function

  //ajax pagination 
  $(function(){
     $('body').on('click', '.pagination a', function(e) { 
      e.preventDefault();
       var url = $(this).attr('href'); 
       var tp = 'GET';
          $.ajax({
             url: url,
             type: tp,
             success: function (data) {
             $('#roles').html(data);
              window.history.pushState("", "", url); 
             },
             error:function(data){
             alert('something went wrong..');
             }
          });//end ajax function
    }); //end of ajax pagination
  });
  </script>
  <script>
     //get add role form
  $(function(){
     $('body').on('click', '.btnAdd', function(e) { 
      e.preventDefault();
      $('#load').hide();
      $('#load_module').hide();
      $('#addFrm').show();
       var url = $(this).attr('href'); 
       var tp = 'GET';
          $.ajax({
             url: url,
             type: tp,
             success: function (data) {
                      
                      if(data.status=='failed'){
                      var message = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h5><i class="icon fa fa-warning"></i> Failed!</h5>'+data.message+'</div>';
                      $('#status').html(message);
                      }
                      else{
                          $('#addFrm').html(data);
                    }
               
             },
             error:function(data){
             alert('something went wrong..');
             }
          });//end ajax function
    
    }); 

  });

//submit add role form and refresh role table
            $('body').on('submit','#form',function (e) {
                e.preventDefault();
                var url = $(this).attr('action');
                var post = $(this).attr('method');
                var data = $(this).serialize();
                $.ajax({
                    url: url,
                    type: post,
                    data: data,
                    success: function (data) {
                        if(data.status=='success'){
                        var message = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h5><i class="icon fa fa-check"></i> Success!</h5>'+data.message+'</div>';
                      }
                        else if(data.status=='failed'){
                            var message = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h5><i class="icon fa fa-warning"></i> Failed!</h5>'+data.message+'</div>';
                        }
                        $('#addFrm').html(message);
                        refresh_roles();
                    },

                    error:function(data){
                        var errors=data.responseJSON;
                        console.log(errors.errors);
                        var error='<b>Validation Errors!</b>';
                        
                        $.each(errors.errors,function(key ,value){
                            error += "<li>"+value+"</li>";

                        });
                        var message = "<div class='alert alert-danger dissmis-x'><ul>" + error + "</ul></div>";
                        $('#status').html(message);

                    }
                });
            });
    //refresh roles table
function refresh_roles(){
    var url = '/adminpanel/role/list'
       var tp = 'GET';
          $.ajax({
             url: url,
             type: tp,
             success: function (data) {
             $('#roles').html(data);
               
             },
             error:function(data){
             alert('something went wrong..');
             }
          });//end ajax function
    }
  </script>
  @endsection