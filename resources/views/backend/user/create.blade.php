@extends('backend.layouts.master')
@section('title','Create User')
@section('content')

    

    <!-- Main content -->
    <section class="content" style="padding-top: 20px;">
    <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">User Register</h3>
                </div>
        <div class="card-body"> 
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
        <div class="col-md-6 offset-3">
        
        <form id="form" action="{{route('user.store')}}" method="post">
         <div id="status"> </div>
                                {{ csrf_field()}}
                                <div class="form-group">
                                    
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Name">
                                    <span class="error"><b>
                                         @if($errors->has('name'))
                                                {{$errors->first('name')}}
                                            @endif</b>
                                        </span>
                                </div>

                                <div class="form-group">
                                    
                                    <input type="email" class="form-control" id="email" name="email" placeholder="E-mail Address">
                                    <span class="error"><b>
                                         @if($errors->has('email'))
                                                {{$errors->first('email')}}
                                            @endif</b>
                                        </span>
                                </div>
                                <div class="form-group">
                                    
                                    <input type="text" class="form-control" id="username" name="username" placeholder="username">
                                    <span class="error"><b>
                                         @if($errors->has('username'))
                                                {{$errors->first('username')}}
                                            @endif</b>
                                        </span>
                                </div>

                                <div class="form-group">
                                    
                                    <input type="password" class="form-control" id="password" name="password" placeholder="password">
                                    <span class="error"><b>
                                         @if($errors->has('password'))
                                                {{$errors->first('password')}}
                                            @endif</b>
                                        </span>
                                </div>
                                <div class="form-group">
                                    
                                   <select class="form-control" id="role" name="role">
                        <option value="">Select Role</option>
                        @foreach($roles as $role)
                            <option value="{{$role->id}}">{{$role->name}}</option>
                        @endforeach
                    </select>
                                </div>

                                <div class="form-group">
                                    
                                     <input type="submit" name="btnSave" id="btnSave" class="btn btn-primary" value="Register User" style="float: right;">
                                </div>          
                            </form>
          <!-- ./col -->
        </div>
        </div>
        </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->

@endsection
@section('scripts')

<script>
	
	$(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CRF-TOKEN': $('meta[name = "csrf-token"]').attr('content')
                }
            });
            $('#form').on('submit', function (e) {
                e.preventDefault();
                var url = $(this).attr('action');
                var post = $(this).attr('method');
                var data = $(this).serialize();
                $.ajax({
                    url: url,
                    type: post,
                    data: data,
                    success: function (data) {
                        if(data.status=='success'){
                        var message = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h5><i class="icon fa fa-check"></i> Success!</h5>'+data.message+'</div>';
                    }
                        else if(data.status=='failed'){
                            var message = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h5><i class="icon fa fa-warning"></i> Failed!</h5>'+data.message+'</div>';
                        }
                        $('#status').html(message);
                    },

                    error:function(data){
                        var errors=data.responseJSON;
                        console.log(errors.errors);
                        var error='<b>Validation Errors!</b>';
                        
                        $.each(errors.errors,function(key ,value){
                            error += "<li>"+value+"</li>";

                        });
                        var message = "<div class='alert alert-danger dissmis-x'><ul>" + error + "</ul></div>";
                        $('#status').html(message);

                    }
                });
            });
        });
</script>

@endsection