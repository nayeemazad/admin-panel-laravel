@extends('backend.layouts.master')
@section('title','User List')
@section('content')
    <!-- Main content -->
    <section class="content" style="padding-top: 20px;">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
        <div class="col-md-12">
        
        <div id="addFrm"> 
                   
        </div>

        <div id="editFrm">
        @include('backend.user._edit')            
        </div>
      
        <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">User List</h3>
                </div>
        <div class="card-body">   
        <a href="{{route('user.create')}}" class="btn btn-primary btn-sm btnAdd" style="float: right;"><i class="fa fa-plus"></i> add </a>
           
        <div id="users">
        @include('backend.user._index')            
        </div>

              </div>
            </div>
        </div>
       </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->

@endsection
@section('scripts')
<script type="text/javascript">
 

  $(function(){
       $.ajaxSetup({
                  headers: {
                      'X-CRF-TOKEN': $('meta[name = "csrf-token"]').attr('content')
                  }
              });
       
//get edit role form
       $('body').on('click', '.btnEdit', function(e) {
                    e.preventDefault();
                    $('#addFrm').hide();
                //var id=$(this).attr('id');
                var id = $(this).closest('tr').children('td:eq(0)').text();
                var url = '/adminpanel/user/edit/'+id;
                var post = 'GET';
                     $.ajax({
                      url: url,
                    type: post,
                    success: function (data) {
                      var message='';
                        $('#editFrm').show();
                        $('#editFrm').html(data);
                        if(data.status=='failed'){
                        message='<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h5><i class="icon fa fa-warning"></i> Failed!</h5>'+data.message+'</div>';
                          $('#editFrm').html(message);
                        }
                    },
                      error:function(data){
                         alert('something went wrong..');
                      }
                  });//end ajax function
    });//end btn click 
  }); //end ready function
  //ajax pagination 
  $(function(){
     $('body').on('click', '.pagination a', function(e) { 
      e.preventDefault();
      $('#editFrm').hide();
      $('#addFrm').hide();
       var url = $(this).attr('href'); 
       var tp = 'GET';
          $.ajax({
             url: url,
             type: tp,
             success: function (data) {
             $('#users').html(data);
              window.history.pushState("", "", url); 
             },
             error:function(data){
             alert('something went wrong..');
             }
          });//end ajax function
    }); 
  });
  //get add user form
  $(function(){
  	 $('body').on('click', '.btnAdd', function(e) { 
  		e.preventDefault();
      $('#editFrm').hide();
      $('#addFrm').show();
  		 var url = $(this).attr('href'); 
  		 var tp = 'GET';
          $.ajax({
             url: url,
             type: tp,
             success: function (data) {
              if(data.status=='failed'){
                      var message = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h5><i class="icon fa fa-warning"></i> Failed!</h5>'+data.message+'</div>';
                      $('#addFrm').html(message);
                      }
                      else{
                          $('#addFrm').html(data);
                    }
               
             },
             error:function(data){
             alert('something went wrong..');
             }
          });//end ajax function
    }); 
  });
//submit edit user role form and refresh user table
$(function(){
            $('body').on('submit','#editRoleFormSubmit',function (e) {
                e.preventDefault();
                var url = $(this).attr('action');
                var post = $(this).attr('method');
                var data = $(this).serialize();
                $.ajax({
                    url: url,
                    type: post,
                    data: data,
                    success: function (data) {
                        if(data.status=='success'){
                        var message = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h5><i class="icon fa fa-check"></i> Success!</h5>'+data.message+'</div>';
                      }
                        else if(data.status=='failed'){
                            var message = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h5><i class="icon fa fa-warning"></i> Failed!</h5>'+data.message+'</div>';
                        }
                        $('#editFrm').html(message);
                        users();
                    },

                    error:function(data){
                        var errors=data.responseJSON;
                        console.log(errors.errors);
                        var error='<b>Validation Errors!</b>';
                        
                        $.each(errors.errors,function(key ,value){
                            error += "<li>"+value+"</li>";

                        });
                        var message = "<div class='alert alert-danger dissmis-x'><ul>" + error + "</ul></div>";
                        $('#error').html(message);

                    }
                });
            });
          });

            //submit add user form and refresh user table
            $('body').on('submit','#form',function (e) {
                e.preventDefault();
                var url = $(this).attr('action');
                var post = $(this).attr('method');
                var data = $(this).serialize();
                $.ajax({
                    url: url,
                    type: post,
                    data: data,
                    success: function (data) {
                        if(data.status=='success'){
                        var message = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h5><i class="icon fa fa-check"></i> Success!</h5>'+data.message+'</div>';
                      }
                        else if(data.status=='failed'){
                            var message = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h5><i class="icon fa fa-warning"></i> Failed!</h5>'+data.message+'</div>';
                        }
                        $('#addFrm').html(message);
                        users();
                    },

                    error:function(data){
                        var errors=data.responseJSON;
                        console.log(errors.errors);
                        var error='<b>Validation Errors!</b>';
                        
                        $.each(errors.errors,function(key ,value){
                            error += "<li>"+value+"</li>";

                        });
                        var message = "<div class='alert alert-danger dissmis-x'><ul>" + error + "</ul></div>";
                        $('#status').html(message);

                    }
                });
            });
    //refresh users table
function users(){
    var url = "{{route('user.list')}}";
       var tp = 'GET';
          $.ajax({
             url: url,
             type: tp,
             success: function (data) {
             $('#users').html(data);
               
             },
             error:function(data){
             alert('something went wrong..');
             }
          });//end ajax function
    }
  </script>


@endsection