<table class="table table-condensed" >
            <thead>
              <tr>
                                    <th>Id</th>
                                    <th>User Name</th>
                                    <th>User Email</th>
                                    <th>User Role</th>
                                    <th>Username</th>
                                    <th>Action</th>
                                    
                                </tr>
                                </thead>
                                <tbody>
                                
                                @foreach($users as $user)
                                    <tr>
                                        <td> {{$user->id}}</td>
                                        <td> {{$user->name}}</td>
                                        <td> {{$user->email}}</td>
                                        <td>  <span class="badge badge-success">@foreach($user->roles as $role)
                                         {{$role->name}}</span>
                                         @endforeach</td>
                                        <td> {{$user->username}}</td>
                                        <td>
                                        <a href="#" class="btn btn-info btn-sm btnEdit"> Change Role</a>
                                        
                                        </td>
                                        
                                    </tr>
                                @endforeach
                                </tbody>
                                {!!$users->links();!!}
                            </table>
                            
              