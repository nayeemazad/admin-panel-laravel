
        @if(isset($user))
         <div id="editFrmPanel" class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Edit Role</h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-widget="collapse">
                    <i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-widget="remove">
                    <i class="fa fa-times"></i>
                  </button>
                </div>
                </div>
        <div class="card-body">
        <div  style="display: block;margin:0 auto;"> 
        <form id="editRoleFormSubmit" action="{{route('user.editRole',$user->id)}}" method="POST">
        <div id="error"></div>
        <b>Edit User Role</b>
                                {{ csrf_field()}}
                                {{method_field('PUT')}}
                                <div class="form-group">
                                    
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="{{$user->name}}" disabled>
                                    <span class="error"><b>
                                         @if($errors->has('name'))
                                                {{$errors->first('name')}}
                                            @endif</b>
                                        </span>
                                </div>

                                <div class="form-group">
                                    
                                    <input type="email" class="form-control" id="email" name="email" placeholder="E-mail Address" value="{{$user->email}}" disabled>
                                    <span class="error"><b>
                                         @if($errors->has('email'))
                                                {{$errors->first('email')}}
                                            @endif</b>
                                        </span>
                                </div>
                                <div class="form-group">
                                    
                                    <input type="text" class="form-control" id="username" name="username" placeholder="username" value="{{$user->username}}" disabled>
                                    <span class="error"><b>
                                         @if($errors->has('username'))
                                                {{$errors->first('username')}}
                                            @endif</b>
                                        </span>
                                </div>
                                <div class="form-group">
                                    
                                   <select class="form-control" id="role" name="role">
                        <option value="">Select Role</option>
                        @foreach($roles as $role)
                            <option value="{{$role->id}}">{{$role->name}}</option>
                        @endforeach
                    </select>
                                </div>

                                <div class="form-group">
                                    
                                     
                                   <input type="submit" class="btn btn-info btn-sm">
                                   <a href="#" class="btn btn-info btn-sm btnCancel"> Cancel</a>
                                </div>
                            </form>
                            </div>
                            </div>
                            </div>
<script>
         //cancel edit form
    $('.btnCancel').click( function () {

    $('#editFrmPanel').hide();
    }); 
</script>
                            @endif
         
          
        