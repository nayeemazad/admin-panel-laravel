    <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">User Register</h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-widget="collapse">
                    <i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-widget="remove">
                    <i class="fa fa-times"></i>
                  </button>
                </div>
                </div>
        <div class="card-body"> 
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
        <div class="col-md-6 offset-3">
        
        <form id="form" action="{{route('user.store')}}" method="post">
         <div id="status"> </div>
                                {{ csrf_field()}}
                                <div class="form-group">
                                    
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Name">
                                    <span class="error"><b>
                                         @if($errors->has('name'))
                                                {{$errors->first('name')}}
                                            @endif</b>
                                        </span>
                                </div>

                                <div class="form-group">
                                    
                                    <input type="email" class="form-control" id="email" name="email" placeholder="E-mail Address">
                                    <span class="error"><b>
                                         @if($errors->has('email'))
                                                {{$errors->first('email')}}
                                            @endif</b>
                                        </span>
                                </div>
                                <div class="form-group">
                                    
                                    <input type="text" class="form-control" id="username" name="username" placeholder="username">
                                    <span class="error"><b>
                                         @if($errors->has('username'))
                                                {{$errors->first('username')}}
                                            @endif</b>
                                        </span>
                                </div>

                                <div class="form-group">
                                    
                                    <input type="password" class="form-control" id="password" name="password" placeholder="password">
                                    <span class="error"><b>
                                         @if($errors->has('password'))
                                                {{$errors->first('password')}}
                                            @endif</b>
                                        </span>
                                </div>
                                <div class="form-group">
                                    
                                   <select class="form-control" id="role" name="role">
                        <option value="">Select Role</option>
                        @foreach($roles as $role)
                            <option value="{{$role->id}}">{{$role->name}}</option>
                        @endforeach
                    </select>
                                </div>

                                <div class="form-group">
                                    
                                     <input type="submit" name="btnSave" id="btnSave" class="btn btn-primary" value="Register User" style="float: right;">
                                </div>          
                            </form>
          <!-- ./col -->
        </div>
        </div>
        </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
      
<script>
    
    
</script>