<!DOCTYPE html>
<html>
<head>
  @include('backend.layouts.partials._styles')
  @yield('css')
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
@include('backend.layouts.partials._topnavbar')
@include('backend.layouts.partials._sidenavbar')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  @if(Session::has('failedMessage'))
            <div class="alert alert-success">
                {{ Session::get('failedMessage') }}
            </div>
        @endif
        
  @yield('content')
  </div>
  <!-- /.content-wrapper -->
@include('backend.layouts.partials._footer')
</div>
<!-- ./wrapper -->
@include('backend.layouts.partials._scripts')
@yield('scripts')
</body>
</html>
