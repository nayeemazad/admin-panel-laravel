
  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
      <img src="{{asset('assets/backend/images/setting.png')}}" alt="Admin panel" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">AdminPanel</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{asset('assets/backend/images/avatar.png')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{Auth::user()->name}}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="true">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
               @php
                             $user = Auth::user();
                             $roles = $user->roles;
                             $module = $roles[0]->modules->sortBy('rank');
                            @endphp
                            @foreach($module as $m)

           <li class="nav-item">
            <a href="{{url($m->slug)}}" <?php echo check_active(request()->path(),$m->slug);?>>
              <img src="{{asset('assets/backend/images/manage.png')}}" width="30px">
              <p>
                {{$m->name}}
               
              </p>
            </a> 
           </li>
                            @endforeach
              <li class="nav-item">
            <a href="{{route('user.logout')}}" class="nav-link">
              <img src="{{asset('assets/backend/images/logout.png')}}" width="30px">
              <p>
                Sign Out
                <i class="right fa fa-angle-left"></i>
              </p>
            </a> 
           </li>
         
          
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>