@if(Session::has('failed'))
        <div class="alert alert-warning alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h5><i class="icon fa fa-warning"></i> Failed!</h5>
                  {{ Session::get('failed') }}</div>
    @endif

    @if(Session::has('success'))
                 <div class="alert alert-success alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h5><i class="icon fa fa-check"></i> Success!</h5>
                  {{ Session::get('success') }}</div>
    @endif