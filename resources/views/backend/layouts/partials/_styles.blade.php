  <title>@yield('title')</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('/assets/backend/dist/css/adminlte.min.css')}}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{asset('assets/backend/plugins/iCheck/flat/blue.css')}}">
  <!-- Morris chart -->
  <link rel="stylesheet" href="{{asset('assets/backend/plugins/morris/morris.css')}}">
  <!-- jvectormap -->
  <link rel="stylesheet" href="{{asset('assets/backend/plugins/jvectormap/jquery-jvectormap-1.2.2.css')}}">
  <!-- Date Picker -->
  <link rel="stylesheet" href="{{asset('assets/backend/plugins/datepicker/datepicker3.css')}}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{asset('assets/backend/plugins/daterangepicker/daterangepicker-bs3.css')}}">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="{{asset('assets/backend/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/backend/plugins/bootstrap/css/bootstrap.css')}}">
  <link rel="stylesheet" href="{{asset('assets/backend/plugins/font-awesome/css/font-awesome.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/backend/plugins/datatables/jquery.dataTables.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/backend/plugins/select2/select2.min.css')}}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <link rel="stylesheet" href="{{asset('assets/backend/plugins/bootstrap/css/bootstrap.css')}}">

<!-- jQuery -->
<script src="{{asset('assets/backend/plugins/jquery/jquery.min.js')}}"></script>


  <style type="text/css">
  .sidebar-dark-primary .nav-sidebar>.nav-item>.nav-link.active{
    background-color: #17a2b8;
  }
    .pagination {
    display: inline-block;    
  }
.pagination li {
    color: black;
    float: left;
    padding: 5px 10px;
    text-decoration: none;
    border: 1px solid #ddd;
    background: #fff;    
}.pagination li a {
    text-decoration: none;
    display: block;
}
.pagination li.active {
    background-color: #17a2b8;
    color: white;
    border: 1px solid #17a2b8;
}
.pagination li:hover:not(.active) {background-color: #ddd;}

.pagination li:first-child {
    border-top-left-radius: 5px;
    border-bottom-left-radius: 5px;
}
.pagination li:last-child {
    border-top-right-radius: 5px;
    border-bottom-right-radius: 5px;

}
  </style>
<script type="text/javascript">
  $(document).ready(function(){
  $(":button,:submit").removeAttr("disabled");  
});
</script>