@extends('backend.layouts.master')
@section('title','Create Permission')
@section('content')

   

    <!-- Main content -->
    <section class="content" style="padding-top: 20px;">
    <div class="card card-info">
                <div class="card-header">
                  <h3 class="card-title">Permission Create</h3>
                  </div>
          <div class="card-body">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
        <div class="col-md-6 offset-3">
        
        <form id="form" action="{{route('permission.store')}}" method="post">
         <div id="status"> </div>
                                {{ csrf_field()}}
                                <div class="form-group">
                                    
                                    <input type="text" class="form-control" id="name" name="name" placeholder="permission name">
                                    <span class="error"><b>
                                         @if($errors->has('name'))
                                                {{$errors->first('name')}}
                                            @endif</b>
                                        </span>
                                </div>
                                <div class="form-group">
                                    
                                    <input type="text" class="form-control" id="slug" name="slug" placeholder="ex: permission/list">
                                    <span class="error"><b>
                                         @if($errors->has('slug'))
                                                {{$errors->first('slug')}}
                                            @endif</b>
                                        </span>
                                </div>

                                <div class="form-group">
                                    
                                     <input type="submit" name="btnSave" id="btnSave" class="btn btn-info" value="Create Permission" style="float: right;">
                                </div>
                                
                               
                                   
                                
                            </form>
         
          
         
          <!-- ./col -->
        </div>
        </div>
        </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->

@endsection
@section('scripts')

<script>
	
	$(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CRF-TOKEN': $('meta[name = "csrf-token"]').attr('content')
                }
            });
            $('#form').on('submit', function (e) {
                e.preventDefault();
                var url = $(this).attr('action');
                var post = $(this).attr('method');
                var data = $(this).serialize();
                $.ajax({
                    url: url,
                    type: post,
                    data: data,
                    success: function (data) {
                        if(data.status=='success'){
                        var message = "<div class='alert alert-success'>" + data.message + "</div>";}
                        else if(data.status=='failed'){
                            var message = "<div class='alert alert-warning'>" + data.message + "</div>";
                        }
                        $('#status').html(message);
                        if(data.redirectUrl){
                          window.location.href = data.redirectUrl;
                        }
                    },

                    error:function(data){
                        var errors=data.responseJSON;
                        console.log(errors.errors);
                        var error='<b>Validation Errors!</b>';
                        
                        $.each(errors.errors,function(key ,value){
                            error += "<li>"+value+"</li>";

                        });
                        var message = "<div class='alert alert-danger dissmis-x'><ul>" + error + "</ul></div>";
                        $('#status').html(message);

                    }
                });
            });
        });
</script>

@endsection