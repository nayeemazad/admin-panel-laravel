<table class="table table-condensed datatables">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Permission Name</th>
                                    <th>Slug</th>
                                    <th>created date</th>
                                    <th>updated date</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($permissions as $permission)
                                    <tr>
                                        <td> {{$permission->id}}</td>
                                        <td> {{$permission->name}}</td>
                                        <td> {{$permission->slug}}</td>
                                        <td> {{$permission->created_at}}</td>
                                        <td> {{$permission->updated_at}}</td>
                                        <td><form action="{{route('permission.delete',$permission->id)}}" method="post">
                                        {{method_field('DELETE')}}
                                        {{ csrf_field()}}
                                        <button class="btn btn-danger btn-sm" type="submit" onclick= "return confirm('are you sure to delete?')"><i class="fa fa-trash"></i></button>
                                        </form>
                                        
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
{!!$permissions->links();!!}
                            </table>