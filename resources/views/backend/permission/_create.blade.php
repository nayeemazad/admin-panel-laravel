    <div class="card card-info">
                <div class="card-header">
                  <h3 class="card-title">Permission Create</h3>
                  <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-widget="collapse">
                    <i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-widget="remove">
                    <i class="fa fa-times"></i>
                  </button>
                </div>
                  </div>
          <div class="card-body">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
        <div class="col-md-6 offset-3">
        
        <form id="form" action="{{route('permission.store')}}" method="post">
         <div id="status"> </div>
                                {{ csrf_field()}}
                                <div class="form-group">
                                    
                                    <input type="text" class="form-control" id="name" name="name" placeholder="permission name">
                                    <span class="error"><b>
                                         @if($errors->has('name'))
                                                {{$errors->first('name')}}
                                            @endif</b>
                                        </span>
                                </div>
                                <div class="form-group">
                                    
                                    <input type="text" class="form-control" id="slug" name="slug" placeholder="ex: permission/list">
                                    <span class="error"><b>
                                         @if($errors->has('slug'))
                                                {{$errors->first('slug')}}
                                            @endif</b>
                                        </span>
                                </div>

                                <div class="form-group">
                                    
                                     <input type="submit" name="btnSave" id="btnSave" class="btn btn-info" value="Create Permission" style="float: right;">
                                </div>                           
                            </form>        
                   <!-- ./col -->
        </div>
        </div>
        </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->